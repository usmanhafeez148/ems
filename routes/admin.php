<?php 

Route::group(['prefix'=>'admin'],function(){
	Auth::routes();
});


Route::group(['prefix'=>'admin'],function(){
	Route::get('/', 'Admin\AdminController@dashboard')->name('admin.dashboard');
	
	Route::get('/category','Admin\AdminController@category')->name('admin.category');
	
	Route::get('/event','Admin\AdminController@event')->name('admin.event');

	Route::get('/country','Admin\AdminController@country')->name('admin.country');
	
	Route::get('/city','Admin\AdminController@city')->name('admin.city');

	
});
