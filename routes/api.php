<?php

// use Illuminate\Http\Request;

Route::resource('/category','Admin\API\CategoryController',[
      'names'=>[
            'index'=>'api.category.list',
            'store'=>'api.category.store',
            'show'=>'api.category.get',
            'update'=>'api.category.update',
            'destroy'=>'api.category.delete',
      ]
]);

Route::resource('/event','Admin\API\EventController',[
      'names'=>[
            'index'=>'api.event.list',
            'store'=>'api.event.store',
            'update'=>'api.event.update',
            'destroy'=>'api.event.delete',
      ]
]);

Route::resource('/city','Admin\API\CityController',[
      'names'=>[
            'index'=>'api.city.list',
            'store'=>'api.city.store',
            'update'=>'api.city.update',
            'destroy'=>'api.city.delete',
      ]
]);

Route::resource('/country','Admin\API\CountryController',[
      'names'=>[
            'index'=>'api.country.list',
            'store'=>'api.country.store',
            'update'=>'api.country.update',
            'destroy'=>'api.country.delete',
      ]
]);