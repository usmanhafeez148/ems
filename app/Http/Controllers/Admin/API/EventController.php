<?php

namespace App\Http\Controllers\Admin\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;

class EventController extends Controller
{
    public function index(){
    	$events = Event::all();
    	return response()->json($events);
    }

    public function store(Request $request)
    {
        
        $event=new Event();
        $event->title=$request->data['title'];
        $event->description=$request->data['description'];
        $event->start_date=$request->data['start_date'];
        $event->end_date=$request->data['end_date'];
        $event->location=$request->data['location'];
        $event->no_of_tickets=$request->data['no_of_tickets'];
        $event->image=$request->data['image'];
        $event->city_id=$request->data['city_id'];
        
        $event->save();

        return response()->json($event);
    }

    public function update(Request $request,$id){
        $event=Event::find($id);
        $event->title=$request->data['title'];
        $event->description=$request->data['description'];
        $event->start_date=$request->data['start_date'];
        $event->end_date=$request->data['end_date'];
        $event->location=$request->data['location'];
        $event->no_of_tickets=$request->data['no_of_tickets'];
        $event->image=$request->data['image'];
        $event->city_id=$request->data['city_id'];
        
        $event->save();

        return response()->json($event);
    }
    public function destroy($id){
        if($event=Event::find($id)){
            if(Event::find($id)->delete()){
                return response()->json($event);
            }
        } 
    }
}
