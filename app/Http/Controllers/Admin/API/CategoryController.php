<?php

namespace App\Http\Controllers\Admin\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EventCategory;

class CategoryController extends Controller
{
    public function index(){
    	$categories = EventCategory::all();
    	return response()->json($categories);
    }

    public function show($id){
        $category = EventCategory::find($id);
        return response()->json($category);
    }
    public function store(Request $request){
        
        $category=new EventCategory();
        $category->title=$request->data['title'];
        $category->description=$request->data['description'];
        $category->save();

        return response()->json($category);
    }

    public function update(Request $request,$id){
        $category=EventCategory::find($id);
        $category->title=$request->data['title'];
        $category->description=$request->data['description'];
        $category->save();

        return response()->json($category);
    }
    public function destroy($id){
    
        if($category=EventCategory::find($id)){
            if(EventCategory::find($id)->delete()){
                return response()->json($category);
            }
        }
    }
}
