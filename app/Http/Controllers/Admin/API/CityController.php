<?php

namespace App\Http\Controllers\Admin\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;

class CityController extends Controller
{
    public function index(){
    	$city = City::all();
    	return response()->json($city);
    }

    public function store(Request $request){
        $city=new City();
        $city->name=$request->data['name'];
        $city->long=$request->data['long'];
        $city->lat=$request->data['lat'];
        $city->country_id=$request->data['country_id'];
        $city->save();

        return response()->json($city);
    }

    public function update(Request $request,$id){
        $city=City::find($id);
        $city->name=$request->data['name'];
        $city->long=$request->data['long'];
        $city->lat=$request->data['lat'];
        $city->country_id=$request->data['country_id'];
        $city->save();

        return response()->json($city);
    }
    public function destroy($id){
        if($city=City::find($id)){
            if(City::find($id)->delete()){
                return response()->json($city);
            }
        }
    }
}
