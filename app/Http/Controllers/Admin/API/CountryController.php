<?php

namespace App\Http\Controllers\Admin\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;

class CountryController extends Controller
{
    public function index(){
    	$country = Country::all();
    	return response()->json($country);
    }

     public function show($id){
        $country = Country::find($id);
        return response()->json($country);
    }
    public function store(Request $request)
    {
        $country=new Country();
        $country->name=$request->data['name'];
        $country->long=$request->data['long'];
        $country->lat=$request->data['lat'];

        $country->save();

        return response()->json($country);
    }

    public function update(Request $request,$id)
    {
        $country=Country::find($id);
        $country->name=$request->data['name'];
        $country->long=$request->data['long'];
        $country->lat=$request->data['lat'];
        
        $country->save();

        return response()->json($country);
    }
    public function destroy($id){
        if($country=Country::find($id)){
            if(Country::find($id)->delete()){
                return response()->json($country);
            }
        }   
    }
}
