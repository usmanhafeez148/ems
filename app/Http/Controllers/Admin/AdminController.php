<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	// public function index(){
	// 	return view('admin.index');
	// }

    public function dashboard(){
    	return view('admin.index');
    }

    public function category(){
        return view('admin.pages.categories');
    }

    public function event(){
    	return view('admin.pages.events');
    }

    public function country(){

        return view('admin.pages.countries');

         }

    public function city(){
        return view('admin.pages.cities');

    }
}
