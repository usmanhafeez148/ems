<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table="events";
    protected $fillable=[
        'title','category_id','location','city_id','start_date','end_date','image','description','no_of_tickets'
    ];
}
