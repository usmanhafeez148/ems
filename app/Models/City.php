<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $table="cities";

    protected $fillable=[
        'name','long','lat','country_id'
    ];

}
