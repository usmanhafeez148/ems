<!doctype html>
<html lang="{{ app()->getLocale() }}">
      <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            
            <!-- Fonts -->
            <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
            @stack('before_styles')
            @include('admin.layouts.header_scripts')
            @stack('after_styles')
      </head>
      <body class="hold-transition skin-blue sidebar-mini">
            <div class="wrapper">
                  @include('admin.layouts.header')
                  @include('admin.layouts.sidebar')
                  <div class="content-wrapper">
                        @yield('content')
                  </div>
                  @include('admin.layouts.footer')
                  @include('admin.layouts.control')
                  <!-- <div id="root">

                  </div> -->
            </div>
            @stack('before_script')
            @include('admin.layouts.footer_scripts')
            @stack('after_scripts')
    </body>
</html>
