<header class="main-header" style="z-index:1000;">
    <!-- Logo -->
    <a href="{{route('admin.dashboard')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>M</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Event</b>Managemet</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    @include('admin.layouts.header_nav')
</header>