<ul class="sidebar-menu" data-widget="tree">
      <!-- <li class="header">MAIN NAVIGATION</li> -->
      
      <li class="{{Route::is('admin.dashboard')?'active':''}}"><a href="{{route('admin.dashboard')}}"><i class="fa fa-circle-o"></i> Dashboard</a></li>

      <li class="{{Route::is('admin.country')?'active':''}}"><a href="{{route('admin.country')}}"><i class="fa fa-circle-o"></i> Country</a></li>

      <li class="{{Route::is('admin.city')?'active':''}}"><a href="{{route('admin.city')}}"><i class="fa fa-circle-o"></i> City</a></li>

      <li class=""><a href=""><i class="fa fa-circle-o"></i> Users and Roles</a></li>

      <li class="{{Route::is('admin.event')?'active':''}}"><a href="{{ route('admin.event')}}"><i class="fa fa-circle-o"></i> Event</a></li>

      <li class="{{Route::is('admin.category')?'active':''}}"><a href="{{ route('admin.category')}}"><i class="fa fa-circle-o"></i> Category</a></li>
      <li class=""><a href=""><i class="fa fa-circle-o"></i> Subscription Plans</a></li>
      <li class=""><a href=""><i class="fa fa-circle-o"></i> Invoices</a></li>
                  
      <!-- <li class="active treeview">
            <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                  </span>
            </a>
            <ul class="treeview-menu">
                  <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                  <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
            </ul>
      </li> -->

      
</ul>