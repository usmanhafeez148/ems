import React from 'react';
import { Route } from "react-router-dom";
import Products from "../Components/Pages/Products/Products";
import Dashboard from "../Components/Pages/Dashboard/dashboard";
const CustomRoutes = ()=>(
      <div>
            <div>
                  <Route exact path="/" component={Dashboard}/>
                  <Route path="/products" component={Products}/>
            </div>
      
      </div>
)

export default CustomRoutes;