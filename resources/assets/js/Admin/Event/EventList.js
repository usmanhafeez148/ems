import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AddEvent from "./AddEvent";
import UpdateEvent from "./UpdateEvent";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

// import registerServiceWorker from './registerServiceWorker';
import axios from "axios";
export default class EventList extends Component {
	constructor(props){
		super(props);

		this.state={
			events:[
			]
		};
		this.newEventHandler=this.newEventHandler.bind(this);
		this.updateEventHandler = this.updateEventHandler.bind(this);
		this.deleteEventHandler = this.deleteEventHandler.bind(this);

	}

	newEventHandler(event){
		let cat=this.state.events;
		cat.push(event);
		this.setState({
			events:cat
		})
	}
	updateEventHandler(event) {

		var index = this.state.events.findIndex(x => x.id === event.id);
		if (index === -1) {

		} else {
			this.setState({
				events: [
					...this.state.events.slice(0, index),
					Object.assign({}, this.state.events[index], event),
					...this.state.events.slice(index + 1)
				]
			});
		}
	}
	deleteEventHandler(e) {
		const id = e.target.id;
		axios({
			url: '/api/event/' + id,
			method: "DELETE",
			data: {
				data: {
					id: id
				}
			}
		}).then((resp) => {
			let eventArray = [...this.state.events]; // make a separate copy of the array
			let index = eventArray.findIndex(x => x.id === resp.data.id);
			if (index === -1) {

			} else {
				eventArray.splice(index, 1);

				this.setState({
					events: []
				});

				this.setState({
					events: eventArray
				});
			}

		});
	}
	componentDidMount(){
		axios.get('/api/event').then((resp)=>{
			this.setState({
				events:resp.data
			})
		});
	}
	render() {
		return (
			<section className="content">
				<div className="row">
					
				</div>
				<br/>
				<div className="row">
					<div className="col-sm-12">
						<div className="box">
							<div className="box-header">
								<h3 className="box-title">Events</h3>
							</div>
							<div className="box-body">
								<div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
									
									<div className="row">
										<div className="col-sm-6">
											<AddEvent newEvent={this.newEventHandler} />
										</div>
										<div className="col-sm-12">
											<table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
												<thead>
													<tr>
														<th>Id</th>
														<th>Title</th>
														<th>Location</th>
														<th>city_id</th>
														<th>Start date</th>
														<th>End date</th>
														<th>Image</th>
														<th>Description</th>
														<th>NO of Tickets</th>
														<th>Edit</th>
														<th>Delete</th>

													</tr>
												</thead>
												<tbody>
													{this.state.events.map((event,idx)=>{
														return (idx%2==0)?
															<tr key={idx} role="row" >
																<td>
																	{event.id}
																</td>
																<td>
																	{event.title}
																</td>
																<td>
																	{event.location}
																</td>
																<td>
																	{event.city_id}
																</td>
																<td>
																	{event.start_date}
																</td>
																<td>
																	{event.end_date}
																</td>
																<td>
																	{event.image}
																</td>
																<td>
																	{event.description}
																</td>
																<td>
																	{event.no_of_tickets}
																</td>
																<td>
																	<UpdateEvent
																		updateEvent={this.updateEventHandler}
																		eventId={event.id}
																		eventTitle={event.title}
																		eventLocation={event.location}
																		eventCityId={event.city_id}
																		eventStartDate={event.start_date}
																		eventEndDate={event.end_date}
																		eventImage={event.image}
																		eventDescription={event.description}
																		eventNoOfTickets={event.no_of_tickets}
																	/>
																</td>
																<td>
																	<Button color="red" onClick={this.deleteEventHandler} id={event.id}>Delete</Button>
																</td>
															</tr>
															:
													 		<tr key={idx} role="row" className="odd">
																<td>
																	{event.id}
																</td>
																<td>
																	{event.title}
																</td>
																<td>
																	{event.location}
																</td>
																<td>
																	{event.city_id}
																</td>
																<td>
																	{event.start_date}
																</td>
																<td>
																	{event.end_date}
																</td>
																<td>
																	{event.image}
																</td>
																<td>
																	{event.description}
																</td>
																<td>
																	{event.no_of_tickets}
																</td>
																<td>
																	<UpdateEvent
																		updateEvent={this.updateEventHandler}
																		eventId={event.id}
																		eventTitle={event.title}
																		eventLocation={event.location}
																		eventCityId={event.city_id}
																		eventStartDate={event.start_date}
																		eventEndDate={event.end_date}
																		eventImage={event.image}
																		eventDescription={event.description}
																		eventNoOfTickets={event.no_of_tickets}
																	/>
																</td>
																<td>
																	<Button color="red" onClick={this.deleteUpdateHandler} id={event.id}>Delete</Button>
																</td>
															</tr>
														
														})}
												</tbody>
												<tfoot>
													<tr>
														<th>Id</th>
														<th>Title</th>
														<th>Location</th>
														<th>city_id</th>
														<th>Start date</th>
														<th>End date</th>
														<th>Image</th>
														<th>Description</th>
														<th>NO of Tickets</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
				
												</tfoot>
											</table>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

if (document.getElementById('event')) {
	ReactDOM.render(<EventList />, document.getElementById('event'));
}
