import React from 'react';
import Axios from 'axios';
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

class AddEvent extends React.Component{

	constructor(props){
		super(props);
		this.state={
			id:'',
			title:'',
			location:'',
			city_id:'',
			start_date:'',
			end_date:'',
			image:'',
			description:'',
			no_of_tickets:'',
		};
		this.handleInputChange=this.handleInputChange.bind(this);
            this.addEvent=this.addEvent.bind(this);

	}
	handleInputChange(e)
	{
		console.log(e);
		this.setState
		({
			[e.target.name]:e.target.value
		});
	}
	addEvent(e)
	{
		axios({
			url:'/api/event',
			method:'POST',
			data:{
				data:this.state
			},
			headers:{
				'content-type':'application/json'
			}
		}).then((resp)=>{
			console.log(resp.data);
			this.props.newEvent(resp.data);
            this.unsetStates();
		});
	}
	unsetStates(){
            this.setState({
            id:'',
			title:'',
			location:'',
			city_id:'',
			start_date:'',
			end_date:'',
			image:'',
			description:'',
			no_of_tickets:'',
            })
      }

	 render(){
            
            return (<div>
                  <Modal style={{ zIndex: 1232, height:"600px" }} trigger={<Button primary>Add Event</Button>} closeIcon>
                        <Modal.Header>Add Category</Modal.Header>
                        <Modal.Content>
                              <div className="form-horizontal">
                                    <div className="box-body">
                                          <div className="form-group">
                                                <label htmlFor="title" className="col-sm-2 control-label">Title</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.title} onChange={this.handleInputChange} type="text" className="form-control" id="title" placeholder="Title" name="title" />
                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="Location" className="col-sm-2 control-label">Location</label>
                                                <div className="col-sm-10">
                                                      <textarea className="form-control" id="location" placeholder="Location" onChange={this.handleInputChange} name="location" value={this.state.location} />
                                                      
                                          </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="City" className="col-sm-2 control-label">City</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.city_id} onChange={this.handleInputChange} type="text" className="form-control" placeholder="City" name="city_id" />
                                                                                                           
                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="Start Date" className="col-sm-2 control-label">Start Date</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.start_date} onChange={this.handleInputChange} type="text" className="form-control" placeholder="Start Date" name="start_date" />
                                                                                                            
                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="End Date" className="col-sm-2 control-label">End Date</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.end_date} onChange={this.handleInputChange} type="text" className="form-control" placeholder="End Date" name="end_date" />
                                                                                                            
                                                </div>
                                          </div>

                                          <div className="form-group">
                                                <label htmlFor="Image" className="col-sm-2 control-label">Image</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.image} onChange={this.handleInputChange} type="text" className="form-control" placeholder="Image" name="image" />
                                                                                                            
                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="Description" className="col-sm-2 control-label">Description</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.description} onChange={this.handleInputChange} type="text" className="form-control" placeholder="Description" name="description" />
                                                                                                            
                                                </div>
                                          </div>

                                          <div className="form-group">
                                                <label htmlFor="NO of Tickets" className="col-sm-2 control-label">NO of Tickets</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.no_of_tickets} max="3" onChange={this.handleInputChange} type="text" className="form-control" placeholder="NO of Tickets" name="no_of_tickets" />
                                                                                                            
                                                </div>

                                          </div>

                                    </div>
                                    <div className="box-footer">
                                          <button onClick={this.addEvent} className="btn btn-info pull-right">Add Event</button>
                                    </div>

                              </div>
                        </Modal.Content>
                  </Modal>
                  
            </div>)
      }

}
export default AddEvent;