import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AddCountry from "./AddCountry";
import UpdateCountry from "./UpdateCountry";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

// import registerServiceWorker from './registerServiceWorker';
import axios from "axios";

export default class CountryList extends Component {
	constructor(props){
		super(props);

		this.state={
			countries:[
			]
		};
		this.newCountryHandler=this.newCountryHandler.bind(this);
		this.updateCountryHandler=this.updateCountryHandler.bind(this);
		this.deleteCountryHandler = this.deleteCountryHandler.bind(this);
	}
	newCountryHandler(country){
		const cat=this.state.countries;
		cat.push(country);
		this.setState({
			countries:cat
		})
	}

	updateCountryHandler(country){
    
		var index = this.state.countries.findIndex(x => x.id === country.id);
		if (index === -1){

		}else{
			this.setState({
				countries: [
					...this.state.countries.slice(0, index),
					Object.assign({}, this.state.countries[index], country),
					...this.state.countries.slice(index + 1)
				]
			});
		}
	}
	deleteCountryHandler(e){
		const id=e.target.id;
		axios({
			url: '/api/country/' + id,
			method: "DELETE",
			data:{
				data:{
					id:id
				}
			}
		}).then((resp)=>{
			let countArray = [...this.state.countries]; // make a separate copy of the array
      let index = countArray.findIndex(x => x.id === resp.data.id);
			if(index===-1){

			}else{
				countArray.splice(index, 1);

				this.setState({ 
					countries: [] 
				});

				this.setState({
          countries: countArray
				});
			}
			
		});
	}
	componentDidMount(){
		axios.get('/api/country').then((resp)=>{
			this.setState({
				countries:resp.data
			})
		});
	}
	render() {
		return (
			<section className="content">
				<div className="row">
					<div className="col-sm-6">
						<AddCountry newCountry={this.newCountryHandler} />
					</div>
				</div>
				<br/>
				<div className="row">
					<div className="col-sm-12">
						<div className="box">
							<div className="box-header">
								<h3 className="box-title">Countries</h3>
							</div>
							<div className="box-body">
								<div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
									
									<div className="row">
										<div className="col-sm-12">
											<table id="example1" className="table table-bordered table-striped" role="grid" aria-describedby="example1_info">
												<thead>
													<tr role="row">
														<th>Id</th>
														<th>Name</th>
														<th>Long</th>
														<th>Lat</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													{this.state.countries.map((country,idx)=>{
														return (idx%2==0)?
															<tr key={idx} className="even">
																<td>
																	{country.id}
																</td>
																<td>
																	{country.name}
																</td>
																<td>
																	{country.long}
																</td>
																<td>
																	{country.lat}
																</td>
																
																<td>
																	<UpdateCountry 
																		updateCountry={this.updateCountryHandler}
																		countId={country.id}
																		countName={country.name}
																		countLong={country.long}
																		countLat={country.lat}
                                    />
																</td>
																<td>
																	<Button color="red" onClick={this.deleteCountryHandler} id={country.id}>Delete</Button>
																</td>
															</tr>
															:
															<tr key={idx} className="odd">
                                <td>
                                  {country.id}
                                </td>
                                <td>
                                  {country.name}
                                </td>
                                <td>
                                  {country.long}
                                </td>
                                <td>
                                  {country.lat}
                                </td>

                                <td>
                                  <UpdateCountry
                                    updateCountry={this.updateCountryHandler}
                                    countId={country.id}
                                    countName={country.name}
                                    countLong={country.long}
                                    countLat={country.lat}
                                  />
                                </td>
                                <td>
                                  <Button color="red" onClick={this.deleteCountryHandler} id={country.id}>Delete</Button>
                                </td>
															</tr>
														
														})
													}
												</tbody>
												<tfoot>
													<tr>
														<th>Id</th>
														<th>Name</th>
														<th>Long</th>
														<th>Lat</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

if (document.getElementById('country')) {
	ReactDOM.render(<CountryList />, document.getElementById('country'));
}
