import React from 'react';
import axios from "axios";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

class AddCountry extends React.Component {
      constructor(props) {
            super(props);
            this.state = {
                  id: '',
                  name: '',
                  long: '',
                  lat: '',
                  
            };
            this.handleInputChange = this.handleInputChange.bind(this);
            this.addCountry = this.addCountry.bind(this);
      }
      unsetStates() {
            this.setState({
                  id: '',
                  name: '',
                  long: '',
                  lat: '',
            })
      }
      
      handleInputChange(e) {
            this.setState({
                  [e.target.name]: e.target.value
            });
      }
      
      addCountry(e) {
            e.preventDefault();
            // let newCat={};
            
            axios({
                  url: '/api/country',
                  method: 'POST',
                  data: {
                        data: this.state
                  },
                  headers: {
                        'content-type': 'application/json'
                  }
            }).then((resp) => {
                  this.props.newCountry(resp.data);
                  this.unsetStates();
            });
            
      }
      
      render() {
            return (<div>
                  <Modal style={{ zIndex: 1232, height: "600px" }} trigger={<Button primary>Add Country</Button>} closeIcon>
                        <Modal.Header>Add Country</Modal.Header>
                        <Modal.Content>
                              <div className="form-horizontal">
                                    <div className="box-body">
                                          <div className="form-group">
                                                <label htmlFor="name" className="col-sm-2 control-label">Name</label>
                                                <div className="col-sm-10">
                                                      <input onChange={this.handleInputChange} type="text" className="form-control" id="name" placeholder="Name" name="name" value={this.state.name} />
                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="long" className="col-sm-2 control-label">Longitude</label>
                                                <div className="col-sm-10">
                                                      <input className="form-control" id="long" placeholder="Longitude" onChange={this.handleInputChange} name="long" value={this.state.long} />

                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="lat" className="col-sm-2 control-label">Latitude</label>
                                                <div className="col-sm-10">
                                                      <input className="form-control" id="lat" placeholder="Latitude" onChange={this.handleInputChange} name="lat" value={this.state.lat} />

                                                </div>
                                          </div>

                                    </div>
                                    <div className="box-footer">
                                          <button onClick={this.addCountry} className="btn btn-info pull-right">Add Country</button>
                                    </div>

                              </div>
                        </Modal.Content>
                  </Modal>

            </div>)
      }
}
      
      export default AddCountry;