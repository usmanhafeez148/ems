import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AddCity from "./AddCity";
import UpdateCity from "./UpdateCity";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

// import registerServiceWorker from './registerServiceWorker';
import axios from "axios";
export default class CityList extends Component {
	constructor(props){
		super(props);

		this.state={
			cities:[
			]
		};
		this.newCityHandler=this.newCityHandler.bind(this);
		this.updateCityHandler=this.updateCityHandler.bind(this);
		this.deleteCityHandler = this.deleteCityHandler.bind(this);
	}
	newCityHandler(city){
		const cat=this.state.cities;
		cat.push(city);
		this.setState({
			cities:cat
		})
	}

	updateCityHandler(city){
		var index = this.state.cities.findIndex(x => x.id === city.id);
		if (index === -1){

		}else{
			this.setState({
				cities: [
					...this.state.cities.slice(0, index),
					Object.assign({}, this.state.cities[index], city),
					...this.state.cities.slice(index + 1)
				]
			});
		}
	}
	deleteCityHandler(e){
		const id=e.target.id;
		axios({
			url: '/api/city/' + id,
			method: "DELETE",
			data:{
				data:{
					id:id
				}
			}
		}).then((resp)=>{
			let citArray = [...this.state.cities]; // make a separate copy of the array
			let index = citArray.findIndex(x => x.id === resp.data.id);
			if(index===-1){

			}else{
				citArray.splice(index, 1);

				this.setState({ 
					cities: [] 
				});

				this.setState({
					cities:citArray
				});
			}
			
		});
	}
	componentDidMount(){
		axios.get('/api/city').then((resp)=>{
			this.setState({
				cities:resp.data
			})
		});
	}
	render() {
		return (
			<section className="content">
				<div className="row">
					<div className="col-sm-6">
						<AddCity newCity={this.newCityHandler} />
					</div>
				</div>
				<br/>
				<div className="row">
					<div className="col-sm-12">
						<div className="box">
							<div className="box-header">
								<h3 className="box-title">Cities</h3>
							</div>
							<div className="box-body">
								<div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
									
									<div className="row">
										<div className="col-sm-12">
											<table id="example1" className="table table-bordered table-striped" role="grid" aria-describedby="example1_info">
												<thead>
													<tr role="row">
														<th>Id</th>
														<th>Name</th>
														<th>Long</th>
														<th>Lat</th>
														<th>Country Id</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													{this.state.cities.map((city,idx)=>{
														return (idx%2==0)?
															<tr key={idx} className="even">
																<td>
																	{city.id}
																</td>
																<td>
																	{city.name}
																</td>
																<td>
																	{city.long}
																</td>
																<td>
																	{city.lat}
																</td>
																<td>
																	{city.country_id}
																</td>
																<td>
																	<UpdateCity 
																		updateCity={this.updateCityHandler}
																		citId={city.id}
																		citName={city.name}
																		citLong={city.long}
																		citLat={city.lat}
																		citCountryId={city.country_id}
																		/>
																</td>
																<td>
																	<Button color="red" onClick={this.deleteCityHandler} id={city.id}>Delete</Button>
																</td>
															</tr>
															:
															<tr key={idx} className="odd">
																<td>
																	{city.id}
																</td>
																<td>
																	{city.name}
																</td>
																<td>
																	{city.long}
																</td>
																<td>
																	{city.lat}
																</td>
																<td>
																	{city.country_id}
																</td>
																<td>
																	<UpdateCity
																		updateCity={this.updateCityHandler}
																		citId={city.id}
																		citName={city.name}
																		citLong={city.long}
																		citLat={city.lat}
																		citCountryId={city.country_id}
																	/>
																</td>
																<td>
																	<Button color="red" onClick={this.deleteCityHandler} id={city.id}>Delete</Button>
																</td>
															</tr>
														
														})
													}
												</tbody>
												<tfoot>
													<tr>
														<th>Id</th>
														<th>Name</th>
														<th>Long</th>
														<th>Lat</th>
														<th>Country Id</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

if (document.getElementById('city')) {
	ReactDOM.render(<CityList />, document.getElementById('city'));
}
