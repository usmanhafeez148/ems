import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AddCategory from "./AddCategory";
import UpdateCategory from "./UpdateCategory";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

// import registerServiceWorker from './registerServiceWorker';
import axios from "axios";
export default class CategoryList extends Component {
	constructor(props){
		super(props);

		this.state={
			categories:[
			]
		};
		this.newCategoryHandler=this.newCategoryHandler.bind(this);
		this.updateCategoryHandler=this.updateCategoryHandler.bind(this);
		this.deleteCategoryHandler = this.deleteCategoryHandler.bind(this);
	}
	newCategoryHandler(category){
		const cat=this.state.categories;
		cat.push(category);
		this.setState({
			categories:cat
		})
	}

	updateCategoryHandler(category){
		var index = this.state.categories.findIndex(x => x.id === category.id);
		if (index === -1){

		}else{
			this.setState({
				categories: [
					...this.state.categories.slice(0, index),
					Object.assign({}, this.state.categories[index], category),
					...this.state.categories.slice(index + 1)
				]
			});
		}
	}
	deleteCategoryHandler(e){
		const id=e.target.id;
		axios({
			url: '/api/category/' + id,
			method: "DELETE",
			data:{
				data:{
					id:id
				}
			}
		}).then((resp)=>{
			let catArray = [...this.state.categories]; // make a separate copy of the array
			let index = catArray.findIndex(x => x.id === resp.data.id);
			if(index===-1){

			}else{
				catArray.splice(index, 1);
				console.log(catArray);

				this.setState({ 
					categories: [] 
				});

				this.setState({
					categories:catArray
				});
			}
			
		});
	}
	componentDidMount(){
		axios.get('/api/category').then((resp)=>{
			this.setState({
				categories:resp.data
			})
		});
	}
	render() {
		return (
			<section className="content">
				<div className="row">
					<div className="col-sm-6">
						<AddCategory newCategory={this.newCategoryHandler} />
					</div>
				</div>
				<br/>
				<div className="row">
					<div className="col-sm-12">
						<div className="box">
							<div className="box-header">
								<h3 className="box-title">Categories</h3>
							</div>
							<div className="box-body">
								<div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
									
									<div className="row">
										<div className="col-sm-12">
											<table id="example1" className="table table-bordered table-striped" role="grid" aria-describedby="example1_info">
												<thead>
													<tr role="row">
														<th>Id</th>
														<th>Title</th>
														<th>Description</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													{this.state.categories.map((category,idx)=>{
														return (idx%2==0)?
															<tr key={idx} className="even">
																<td>
																	{category.id}
																</td>
																<td>
																	{category.title}
																</td>
																<td>
																	{category.description}
																</td>
																<td>
																	<UpdateCategory 
																		updateCategory={this.updateCategoryHandler}
																		catId={category.id}
																		catTitle={category.title}
																		catDescription={category.description}
																		/>
																</td>
																<td>
																	<Button color="red" onClick={this.deleteCategoryHandler} id={category.id}>Delete</Button>
																</td>
															</tr>
															:
															<tr key={idx} className="odd">
																<td>
																	{category.id}
																</td>
																<td>
																	{category.title}
																</td>
																<td>
																	{category.description}
																</td>
																<td>
																	<UpdateCategory
																		updateCategory={this.updateCategoryHandler}
																		catId={category.id}
																		catTitle={category.title}
																		catDescription={category.description}
																	/>
																</td>
																<td>
																	<Button color="red" onClick={this.deleteCategoryHandler} id={category.id}>Delete</Button>
																</td>
															</tr>
														
														})
													}
												</tbody>
												<tfoot>
													<tr>
														<th>Id</th>
														<th>Title</th>
														<th>Description</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

if (document.getElementById('category')) {
	ReactDOM.render(<CategoryList />, document.getElementById('category'));
}
