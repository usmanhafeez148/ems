import React from 'react';
import axios from "axios";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';

class UpdateCategory extends React.Component {
      constructor(props) {
            super(props);
            this.state = {
                  id: '',
                  title: '',
                  description: ''
            };
            this.handleInputChange = this.handleInputChange.bind(this);
            this.updateCategoryHandler = this.updateCategoryHandler.bind(this);
            this.EditFormHandler = this.EditFormHandler.bind(this);
      }
      unsetStates() {
            this.setState({
                  id: '',
                  title: '',
                  description: ''
            })
      }

      handleInputChange(e) {
            this.setState({
                  [e.target.name]: e.target.value
            });
      }

      EditFormHandler(e){
            this.setState({
                  id:this.props.catId,
                  title:this.props.catTitle,
                  description:this.props.catDescription,
            })
      }
      updateCategoryHandler(e) {
            e.preventDefault();
            // let newCat={};
            axios({
                  url: '/api/category/' + this.state.id,
                  method: 'PUT',
                  data: {
                        data: this.state
                  },
                  headers: {
                        'content-type': 'application/json'
                  }
            }).then((resp) => {
                  
                  this.props.updateCategory(resp.data);
                  // this.unsetStates();
            });

      }

      render() {

            return (<div>
                  <Modal style={{ zIndex: 1232, height: "300px" }} trigger={<Button color="green" onClick={this.EditFormHandler}>Edit</Button>} closeIcon>
                        <Modal.Header>Update Category</Modal.Header>
                        <Modal.Content>
                              <div className="form-horizontal">
                                    <div className="box-body">
                                          <div className="form-group">
                                                <label htmlFor="title" className="col-sm-2 control-label">Title</label>
                                                <div className="col-sm-10">
                                                      <input value={this.state.title} onChange={this.handleInputChange} type="text" className="form-control" id="title" placeholder="Title" name="title" />
                                                </div>
                                          </div>
                                          <div className="form-group">
                                                <label htmlFor="description" className="col-sm-2 control-label">Description</label>
                                                <div className="col-sm-10">
                                                      <textarea className="form-control" id="description" placeholder="Description" onChange={this.handleInputChange} name="description" value={this.state.description} />

                                                </div>
                                          </div>
                                    </div>
                                    <div className="box-footer">
                                          <button onClick={this.updateCategoryHandler} className="btn btn-info pull-right">Update Category</button>
                                    </div>

                              </div>
                        </Modal.Content>
                  </Modal>

            </div>)
      }
}

export default UpdateCategory;