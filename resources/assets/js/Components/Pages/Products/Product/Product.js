import React from 'react';

const Product = (props) => (
      
      <tr>
            <td>{props.id}</td>
            <td>{props.name}</td>
            <td>{props.title}</td>
            <td>{props.price}</td>
      </tr>
      
)

export default Product;