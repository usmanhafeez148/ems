import React, { Component } from 'react';
// import './App.css';
import Product from "./Product/Product";
import ProductsTitle from "./products_title";
class Products extends Component {

      render() {
            return (
                  <div>
                        <section className="content-header">
                              <h1>
                                    Products
                                    <small className="badge-info">4</small>
                              </h1>
                              {/* <ol className="breadcrumb">
                                    <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
                                    <li><a href="#">Tables</a></li>
                                    <li className="active">Data tables</li>
                              </ol> */}
                        </section>
                        <section className="content">
                              <div className="row">
                                    <div className="col-xs-12">
                                          <div className="box">
                                                <div className="box-header">
                                                      <h3 className="box-title">Hover Data Table</h3>
                                                </div>
                                                <div className="box-body">
                                                      <table id="example2" className="table table-bordered table-hover">
                                                            <thead>
                                                                  <ProductsTitle />
                                                            </thead>
                                                            <tbody>
                                                                  <Product id={1} name="tv" title="sonny" price={4500}/>
                                                            </tbody>
                                                            <tfoot>
                                                                  <ProductsTitle />
                                                            </tfoot>
                                                      </table>
                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </section>
                  </div>
            );
      }
}

export default Products;
