import React, { Component } from 'react';
// import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import  LayoutsHead  from "./Layouts/Head/layouts_head";
import  LayoutsFoot  from "./Layouts/Foot/layouts_foot";
import  LayoutsControl  from "./Layouts/Control/layouts_control";
import  LayoutsSide  from "./Layouts/Side/layouts_side";
import  CustomRoutes  from "../Routers/router";

class App extends Component {
  render() {
    return (
      <Router>
      <div>
        <LayoutsHead />
        <LayoutsSide />
        <div className="content-wrapper">
          <CustomRoutes>
          </CustomRoutes>
        </div>
        <LayoutsFoot />
        <LayoutsControl />
      </div>
      </Router>
    );
  }
}

export default App;
