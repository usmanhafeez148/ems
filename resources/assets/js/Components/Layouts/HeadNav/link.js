import React from 'react';
import SubLink from "./sub_link";
class Link extends React.Component {
      render() {
            return (
                  <li className={this.props.className}>
                        <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                              <i className={this.props.icon}></i>
                              <span className="label label-success">4</span>
                        </a>
                        
                  </li>);
      }
}

export default Link;