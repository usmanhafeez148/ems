import React from 'react';

class SubLink extends React.Component {
      render() {
            return (<ul className="dropdown-menu">
                  <li className="header">You have 4 messages</li>
                  <li>
                        <ul className="menu">
                              <li>
                                    <a href="#">
                                          <div className="pull-left">
                                                <img src="dist/img/user2-160x160.jpg" className="img-circle" alt="User Image" />
                                          </div>
                                          <h4>
                                                Support Team<small><i className="fa fa-clock-o"></i> 5 mins</small>
                                          </h4>
                                          <p>Why not buy a new awesome theme?</p>
                                    </a>
                              </li>
                              <li>
                                    <a href="#">
                                          <div className="pull-left">
                                                <img src="dist/img/user4-128x128.jpg" className="img-circle" alt="User Image" />
                                          </div>
                                          <h4>
                                                Reviewers<small><i className="fa fa-clock-o"></i> 2 days</small>
                                          </h4>
                                          <p>Why not buy a new awesome theme?</p>
                                    </a>
                              </li>
                        </ul>
                  </li>
                  <li className="footer"><a href="#">See All Messages</a></li>
            </ul>);
      }
}

export default SubLink;