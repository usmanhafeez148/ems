import React, { Component } from 'react';
import Link from "./link";

class HeadNav extends Component {
      render() {
            
            return (
                  <div>
                        <nav className="navbar navbar-static-top">
                              <a href="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
                                    <span className="sr-only">Toggle navigation</span>
                              </a>
                              <div className="navbar-custom-menu">
                                    <ul className="nav navbar-nav">
                                          <Link className="dropdown messages-menu" icon="fa fa-envelope-o"/>
                                          <Link className="dropdown notifications-menu" icon="fa fa-bell-o"/>
                                          <Link className="dropdown tasks-menu" icon="fa fa-flag-o"/>
                                          <Link className="dropdown user-menu" icon="fa fa-bell-o"/>
                                          <li>
                                                <a href="#" data-toggle="control-sidebar"><i className="fa fa-gears"></i></a>
                                          </li>
                                    </ul>
                              </div>
                        </nav>
                  </div>
            );
      }
}

export default HeadNav;