import React, { Component } from 'react';
// import './layouts_side.css';
import LayoutsNav from "../Nav/layouts_nav";

class LayoutsSide extends Component {
      render() {
            return (
                  <div className="main-sidebar">
                        <aside >
                              <section className="sidebar">
                                    <div className="user-panel">
                                          <div className="pull-left image">
                                                {/* <img src="" className="img-circle" alt="User Image" /> */}
                                          </div>
                                          {/* <div className="pull-left info">
                                                <p>Alexander Pierce</p>
                                                <a href="#">
                                                      <i className="fa fa-circle text-success"></i> Online
                                                </a>
                                          </div> */}
                                    </div>
                                    <form className="sidebar-form">
                                          <div className="input-group">
                                                <input type="text" name="q" className="form-control" placeholder="Search..." />
                                                <span className="input-group-btn">
                                                      <button type="submit" name="search" id="search-btn" className="btn btn-flat">
                                                            <i className="fa fa-search"></i>
                                                      </button>
                                                </span>
                                          </div>
                                    </form>
                                    <LayoutsNav />
                              </section>
                        </aside>
                  </div>
            );
      }
}

export default LayoutsSide;