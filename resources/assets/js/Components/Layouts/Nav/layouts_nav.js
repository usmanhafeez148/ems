import React, { Component } from 'react';
// import './layouts_head.css';
// import { NavLink} from "react-router-dom";
import NavLink from "./nav_link";

import CustomRoutes from "../../../Routers/router";

class LayoutsNav extends Component {
      render() {
            
            return (
                  <ul className="sidebar-menu" data-widget="tree">
                        <li className="header">Event Management System</li>
                        <NavLink to="/" >
                              <i className="fa fa-dashboard"></i> <span>Dashboard</span>
                        </NavLink>
                  
                        <NavLink to="/products">
                              <i className="fa fa-circle-o"></i> <span>Product List</span>
                        </NavLink>
                       
                  </ul>
            );
      }
}

export default LayoutsNav;