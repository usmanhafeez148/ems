import React, { Component } from 'react';
// import './layouts_head.css';
import HeadNav from "../HeadNav/head_nav";
class LayoutsHead extends Component {
      
      render() {
            
            return (
                  <div>
                        <header className="main-header">
                              <a href="#" className="logo">
                                    <span className="logo-mini"><b>E</b>r</span>
                                    <span className="logo-lg"><b>Event</b>ree</span>
                              </a>
                              <HeadNav />
                        </header>
                  </div>
            );
      }
}

export default LayoutsHead;