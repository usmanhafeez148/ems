import React, {Component} from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './Components/App';
// import registerServiceWorker from './registerServiceWorker';

export default class Index extends Component {
      render() {
            return (
                  <div>
                        <App/>
                  </div>
            );
      }
}

if (document.getElementById('root')) {
      ReactDOM.render(<Index />, document.getElementById('root'));
}
// registerServiceWorker();